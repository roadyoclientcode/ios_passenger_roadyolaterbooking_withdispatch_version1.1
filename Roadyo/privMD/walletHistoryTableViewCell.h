//
//  walletHistoryTableViewCell.h
//  RoadyoDispatch
//
//  Created by 3Embed on 23/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface walletHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *paymentStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *paymentReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *walletBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLable;

@end
