//
//  BookingHistoryWithoutDropAddressCell.h
//  RoadyoDispatch
//
//  Created by 3Embed on 01/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingHistoryWithoutDropAddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backgroundView1;
@property (weak, nonatomic) IBOutlet UIView *dummyBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pickUpImageView;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddresslabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;

@end
