//
//  Receipt2TabelViewCell.h
//  RoadyoDispatch
//
//  Created by 3Embed on 26/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Receipt2TabelViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cashCardImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;

@end
